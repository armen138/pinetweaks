#!/bin/ash
if [ ! -f ~/.vim/vimrc ]; then
    echo "Installing vimrc and plugins"
    git clone https://github.com/Armen138/.vim.git ~/.vim
    cd ~/.vim
    ash init.sh
    git submodule update --init --recursive
else
    echo "vimrc exists, skipping vim plugin installation"
fi
