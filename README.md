# pinetweaks
## some controls for things you might want to do on a pinephone

Installers:

* install_deps.sh - installs some requirements
* install_profile_tweaks.sh - installs a file into your /etc/profile.d to add some sensible environment variables
* install_vimrc.sh - installs my vimrc and the plugins that go with it
* install_app.sh - installs a launcher icon to run the app ui
* install.sh - installs all of the above

Scripts:

The util/ folder contains a few bash scripts to do things like bringing the modem online, changing the display scaling, etc.

Suggestions and contributions welcome.

NB.
This will work best if your pinephone runs PostmarketOS with Plasma-Mobile.
