#!/bin/ash
echo "Installing launch icon for PineTweaks app"
sed -i "s|APP_HOME|${PWD}|" app/pinetweaks.desktop
mkdir -p ~/.local/share/applications
cp app/pinetweaks.desktop ~/.local/share/applications/pinetweaks.desktop