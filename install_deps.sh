#!/bin/ash
echo "Installing prerequisites"
sudo apk add py3-qt5 py3-pip vim git
# install system wide ofonoctl so app can find it
sudo pip3 install ofonoctl
pip3 install screeninfo
