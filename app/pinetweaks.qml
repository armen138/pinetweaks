import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
// import Qt 4.7

Kirigami.ApplicationWindow {
    id: root
    width: 480
    height: 800
    
    ColumnLayout { 
        spacing: Kirigami.Units.largeSpacing * 5
        Layout.preferredWidth: Kirigami.Units.gridUnit * 25
        Layout.leftMargin: Kirigami.Units.gridUnit
        Kirigami.Heading {
                    Layout.leftMargin: Kirigami.Units.gridUnit

            Layout.fillWidth: true
            text: "Pinephone Tweaks"
            level: 1
        }
        RowLayout {
            Layout.leftMargin: Kirigami.Units.gridUnit

            Layout.fillWidth: true

            Controls.Label {
                text: "display scale"
            }
            Controls.Slider {
                id: scaleSlider
                Layout.fillWidth: true
                stepSize: 0.5
                to: 3.0
                from: 1.0
                value: scaler.current()
                onMoved: {
                    scaleLabel.text = scaleSlider.value + "x"
                    scaler.set_scale(scaleSlider.value);
                }
            }
            Controls.Label {
                id: scaleLabel
                text: scaler.current() + "x"
            }
            Controls.Button {
                text: "apply"
                onClicked: {
                    commandOutput.text = scaler.apply()

                }
            }
        }
        Controls.Switch {
            Layout.leftMargin: Kirigami.Units.gridUnit
            text: "Modem Power"
            onClicked: {
                commandOutput.text = ofono.toggle_power()
            }
        }
        Controls.Switch {
            Layout.leftMargin: Kirigami.Units.gridUnit
            onClicked: {
                commandOutput.text = ofono.toggle_online()
            }
            text: "Modem Online"
        }       
        Controls.Label {
            Layout.leftMargin: Kirigami.Units.gridUnit
            text: ""
            id: commandOutput
        } 
    }
}