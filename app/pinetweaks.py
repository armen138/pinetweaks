#!/usr/bin/env python3

import sys
import os
import subprocess
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine

from screeninfo import get_monitors

class Ofono(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.is_online = False
        self.is_powered = False

    @pyqtSlot(result=str)
    def toggle_power(self):
        cli = "ofonoctl {}".format("offline" if self.is_online else "online")
        self.is_online = not self.is_online
        ofonoctl = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = ofonoctl.communicate()
        return err.decode("utf-8") if err else out.decode("utf-8")

    @pyqtSlot(result=str)
    def toggle_online(self):
        cli = "ofonoctl {}".format("poweroff" if self.is_powered else "poweron")
        self.is_powered = not self.is_powered
        ofonoctl = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = ofonoctl.communicate()
        return err.decode("utf-8") if err else out.decode("utf-8")
    
class Scaler(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.want_scale = self.current()

    @pyqtSlot(result=float)
    def current(self):
        monitor = get_monitors()[0]
        short_side = monitor.width if monitor.width < monitor.height else monitor.height
        scale = 720 / short_side
        return scale
    
    @pyqtSlot(float)
    def set_scale(self, scale):
        self.want_scale = scale

    @pyqtSlot(result=str)
    def apply(self):
        print("Apply scale: " + str(self.want_scale))
        cli = f"kscreen-doctor \"output.unknown DSI-1-unknown.scale.{self.want_scale}\""
        kscreen_doctor = subprocess.Popen(cli, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = kscreen_doctor.communicate()
        return err.decode("utf-8") if err else out.decode("utf-8")

if __name__ == "__main__":
    scaler = Scaler()
    ofono = Ofono()
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    context = engine.rootContext()
    context.setContextProperty('scaler', scaler)
    context.setContextProperty('ofono', ofono)
    engine.load(os.path.realpath(__file__).replace("py", "qml"))
    app.exec_()