# makes angelfish work on plasma mobile
export QTWEBENGINE_DISABLE_SANDBOX=1
# allows touch scrolling in firefox
export MOZ_USE_XINPUT2=1 
# allows phosh keyboard input on firefox
export MOZ_ENABLE_WAYLAND=1
# just in case the above doesn't do it on its own
export GDK_BACKEND=wayland
# locale stuff for Qt to find
export LANG=en_CA.utf-8
export LC_ALL=en_CA.utf-8
# default editor
export EDITOR=vim

